class ChangeDataTypeForDescription < ActiveRecord::Migration
  def change
  	change_column :dogs, :description, :text
  end
end
