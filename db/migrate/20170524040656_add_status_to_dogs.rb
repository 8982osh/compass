class AddStatusToDogs < ActiveRecord::Migration
  def change
    add_column :dogs, :status, :string
  end
end
