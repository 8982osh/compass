class CreateFosters < ActiveRecord::Migration
  def change
    create_table :fosters do |t|
      t.string :username
      t.string :first_name
      t.string :last_name
      t.string :company
      t.string :address
      t.string :city
      t.string :state
      t.string :zip
      t.string :home_phone
      t.string :mobile_phone
      t.string :email

      t.timestamps null: false
    end
  end
end
