class CreateDogs < ActiveRecord::Migration
  def change
    create_table :dogs do |t|
      t.string :animal_id
      t.string :name
      t.string :gender
      t.integer :weight
      t.string :age
      t.string :breed_one
      t.string :breed_two
      t.string :breed
      t.string :cat_compatible
      t.string :dog_compatible
      t.string :human_compatible
      t.string :energy_level
      t.integer :fee
      t.string :description

      t.timestamps null: false
    end
  end
end
