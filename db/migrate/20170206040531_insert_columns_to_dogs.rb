class InsertColumnsToDogs < ActiveRecord::Migration
  def change
  	add_column :dogs, :breed_type, :string, after: :age
  	add_column :dogs, :temperament, :string, after: :energy_level
  end
end
