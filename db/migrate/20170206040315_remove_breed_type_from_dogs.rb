class RemoveBreedTypeFromDogs < ActiveRecord::Migration
  def change
  	remove_column :dogs, :breed_type, :string
  end
end
