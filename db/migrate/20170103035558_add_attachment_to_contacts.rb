class AddAttachmentToContacts < ActiveRecord::Migration
  def change
    add_column :contacts, :attachment, :string
  end
end
