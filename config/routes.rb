Rails.application.routes.draw do
  

  #Display for foster dogs
  resources :dogs   
    
  #Display for adoptable dogs
  get 'michigan' => 'dogs#michigan'
  post 'michigan' => 'dogs#michigan'

  get 'donate' => 'dogs#donate'

  #Display adoption info and form partial application
  get 'adoption' => 'dogs#adoption'

  devise_for :users
  
  #For Contact Form pg 
  resources :contacts, only: [:new, :create, :destroy]

  #Remove uploaded attachment on Contact Form pg
  delete 'contacts' => 'contacts#destroy'

  #Display Contact Form acceptance to user
  get 'accept' => 'contacts#accept'

  #Displays Dog's Journey pg
  get 'welcome/show'

  get 'about' => 'welcome#about'

  #Displays About Us pg
  post 'about' => 'welcome#about'

  root to: 'welcome#index'

end
