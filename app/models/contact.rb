class Contact < ActiveRecord::Base   

  #extend CarrierWave::Mount
  mount_uploader :image, ImageUploader
	
  attr_accessor :first_name, :last_name, :subject, :email, :description, :attachment, :remove_attachment, :attachment_cache
  
  

  #All fields for contact form 
  validates :first_name, 
            length: { maximum: 30 }, 
            presence: true
  validates :last_name, 
            length: { maximum: 30 }, 
            presence: true
  validates :subject, presence: true
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, 
            presence: true, 
            length: { maximum: 100 }, 
            format: { with: VALID_EMAIL_REGEX }
  #validates :image, presence: ture, if file_content_type: ( allow: 'image/pdf', 'image/docx', 'image/doc', 'image/html' )
  validates :description, presence: true
  
  end
  

