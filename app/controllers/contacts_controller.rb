class ContactsController < ApplicationController

  def new
  	@contact = Contact.new
  end

  def create
  	@contact = Contact.new(contact_params)
  	if @contact.valid?
      #Capitalize first and last names
      @contact.first_name[0] = @contact.first_name[0].capitalize
      @contact.last_name[0] = @contact.last_name[0].capitalize 
      ContactMailer.new_contact(@contact).deliver_now
      @contact.save
  		redirect_to accept_path 
  	else
      flash[:error] = "Error: #{@dog.error.full_messages}"
      render :new
    end
  end


  private
    def contact_params
  	  params.require(:contact).permit(:first_name, :last_name, :subject, :email, 
      :description, :image, :remove_image, :image_cache)
    end
end
