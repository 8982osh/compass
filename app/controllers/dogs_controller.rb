class DogsController < ApplicationController
 
  #Display only foster dogs on index.html.erb
  def index
    @dogs = Dog.foster
  end

  def show
  	@dog = Dog.find(params[:id])
  end

  def new
  	@dog = Dog.new
  end

  def edit
  	@dog = Dog.find(params[:id])
  end

  def create
  	@dog = Dog.new(dog_params)
    if @dog.save
      flash[:notice] = "Dog was saved successfully."
      redirect_to @dog
    else
      flash[:error] = "Error: #{@dog.errors.full_messages}."
      render :new 
    end 
  end

  #Display dogs on michigan.html.erb
  def michigan
    @dogs = Dog.adoptable
  end

  def update
    @dog = Dog.find(params[:id])  
    if @dog.update_attributes(dog_params)
       flash[:notice] = "Update successful."
       redirect_to @dog
    else
      flash[:error] = "Error saving input. Please try again."
      render :edit
    end
  end

  def destroy
  	@dog = Dog.find(dog_params[:id])
  	@dog.destroy
    flash[:notice] = "Dog has been removed."
    redirect_to @dog
  end


  private
  def dog_params
  params.require(:dog).permit(:animal_id, :name, :status, :gender, :weight, :age, :breed_type, :breed_one, :breed_two,
         :cat_compatible, :dog_compatible, :human_compatible, :energy_level, :temperament, :fee, :description, :image)
  end
end
