//Hide secondary_breed if Purebreed radio button clicked

$(document).ready(function(){
    $("#purebreed").click(function(){
        $("breed_two").hide();
    });
    $("#blend").click(function(){
        $("breed_two").show();
    });
});